<?php
/*This file is part of ChildNeve, neve child theme.

All functions of this file will be loaded before of parent theme functions.
Learn more at https://codex.wordpress.org/Child_Themes.

Note: this function loads the parent stylesheet before, then child theme stylesheet
(leave it in place unless you know what you are doing.)
*/


if ( ! function_exists( 'suffice_child_enqueue_child_styles' ) ) {
	function ChildNeve_enqueue_child_styles() {
	    // loading parent style
	    wp_register_style(
	      'parente2-style',
	      get_template_directory_uri() . '/style.css'
	    );

	    wp_enqueue_style( 'parente2-style' );
	    // loading child style
	    wp_register_style(
	      'childe2-style',
	      get_stylesheet_directory_uri() . '/style.css'
	    );
	    wp_enqueue_style( 'childe2-style');
	 }
}
add_action( 'wp_enqueue_scripts', 'ChildNeve_enqueue_child_styles' );


//shortcode
function liste_func(){
	
	global $wpdb;
	$select = "SELECT * FROM pantheon.wp_annuaire ";
	
   $requete = $wpdb->get_results($select);
   ?>
     <table>
                <tr>
                    <th>Entreprise</th>
                    <th>Lieu</th>
                    <th>Prenom</th>
                    <th>Nom</th>
                    <th>email</th>                 
                </tr>
   <?php
   foreach($requete as $ligne){
  ?>
   <tr>
   <td><?php echo $ligne->nom_entreprise;?></td>
   <td><?php echo$ligne->localisation_entreprise;?></td>
   <td><?php echo$ligne->prenom_contact;?></td>
   <td><?php echo$ligne->nom_contact;?></td>
   <td><?php echo$ligne->mail_contact;?></td>

<?php
   }

}

add_shortcode( 'liste', 'liste_func' );


add_action('admin_menu', 'add_links_menu');
function add_links_menu()
{
    add_menu_page('Add-Modif-Entreprise', 'Add-Modif-Entreprise', 'administrator', 'add-modif-entreprise.php', 'page_gen', '', 5);
    // add_submenu_page( 'add-modif-entreprise.php', 'Add_page', 'Add_page', 'administrator', 'add-entreprise.php','page_gen', 2);
}
function page_gen()
{
    include('add-modif-entreprise.php');
}

/*Write here your own functions */




